﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    public Text consoleLog;
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.NickName = $"Player{Random.Range(100, 999)}";
        Log($"{PhotonNetwork.NickName} connected!");
        PhotonNetwork.AutomaticallySyncScene = true;
        PhotonNetwork.GameVersion = "1";
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnJoinedRoom()
    {
        Log("Joined the room!");
        PhotonNetwork.LoadLevel("Game");
    }

    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(null, new Photon.Realtime.RoomOptions {MaxPlayers = 2});
    }

    public override void OnConnectedToMaster()
    {
        Log("Connected to Master!");
    }

    public void JoinRoom()
    {
        PhotonNetwork.JoinRandomRoom();
    }

    private void Log(string message)
    {
        Debug.Log(message);
        consoleLog.text += message;
        consoleLog.text += "\n";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
